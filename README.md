# README #

Gerberfiles for connecting UNBedroomClockESP32 circuit boards together.
ESP32 code for controlling displays, reading temperature and socket server.

### Setup ###

- Install visual studio code
- Install python 3
	- Add python to path
- In VSCode, press Ctrl+Shift+X, search and install "PlatformIO IDE"
- Open PlatformIO from the left bar, select "PIO Home" -> "Open" -> "Open Project", select "ClockThermometerConESP32" folder
- In PlatformIO, open "esp32doit-devkit-v1" -> "General", double click on build to download required libraries and build project
- Connect esp32 to usb, upload install code to esp32
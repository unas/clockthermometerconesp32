#ifndef CTSETTINGS_H
#define CTSETTINGS_H

#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <Adafruit_MCP9808.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "TemperatureType.h"

class CTSettings {
	private:
		// SD-card settings
		String wlanSsid;
		String wlanPassword;
		String ntpTimezone;
		String ntpServer;
		bool temperatureEnabled;
		int temperatureServerPort;
		double powerStartTime[7];
		double powerEndTime[7];
		TemperatureType temperatureType;
		// MySQL Settings
		bool mysqlEnabled;
		IPAddress* mysqlServerAddress;
		String mysqlUsername;
		String mysqlPassword;
		int mysqlPort;
		String mysqlDatabase;
		String mysqlTable;
		bool mysqlNeedsAnUpdate;

		bool setupSD();
		bool getSettings();

	public:
		CTSettings();

		String getWlanSsid();
		String getWlanPassword();
		String getNtpTimezone();
		String getNtpServer();
		bool isTemperatureEnabled();
		int getTemperatureServerPort();
		double* getPowerStartTime();
		double* getPowerEndTime();
		TemperatureType getTemperatureType();
		bool isMysqlEnabled();
		IPAddress* getMysqlServerAddress();
		String getMysqlUsername();
		String getMysqlPassword();
		int getMysqlPort();
		String getMysqlDatabase();
		String getMysqlTable();
		void setMysqlNeedsAnUpdate(bool needAnUpdate);
		bool isMysqlNeedsAnUpdate();
};

#endif
#include <Arduino.h>

#include <ArduinoJson.h>
#include <WiFi.h>
#include <Adafruit_MCP23X17.h>
#include <Adafruit_MCP9808.h>
#include <vector>
#include <math.h>
#include <pthread.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include "CTLibrary.h"

CTLibrary* ctLibrary;

void setup() {
	Serial.begin(115200);
	Serial.println("Setup starting");
	
	ctLibrary = new CTLibrary();

	ctLibrary->updateNTP();
	ctLibrary->printLocalTime();
	ctLibrary->turnOffAllNumbers();
	
	if (ctLibrary->getSettings()->isTemperatureEnabled()) {
		ctLibrary->setTemperatureTypeLetter(ctLibrary->getSettings()->getTemperatureType());
	}
	// Creating temperature server thread
	if (ctLibrary->getSettings()->isTemperatureEnabled()) {
		ctLibrary->createTemperatureServer();
	}

	if (ctLibrary->getSettings()->isMysqlEnabled()) {
		ctLibrary->getSettings()->setMysqlNeedsAnUpdate(ctLibrary->getHoursSinceLastMySQLUpdate() > 0);
	}

	ctLibrary->setPower();
}

void loop() {

	ctLibrary->setPower();
	if (ctLibrary->shouldUpdateNTP()) {
		Serial.println("NTP needs an update");
		ctLibrary->updateNTP();
	}
	ctLibrary->updateDateTimeScreen();
	if (ctLibrary->getSettings()->isTemperatureEnabled()) {
		ctLibrary->updateTemperature();
	}
	if (ctLibrary->getSettings()->isMysqlNeedsAnUpdate() && ctLibrary->getSettings()->isMysqlEnabled()) {
		ctLibrary->updateMySQLTemperatures();
	}

	delay(ctLibrary->millisecondsUntilNextSecond());
}
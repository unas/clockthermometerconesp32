#ifndef CTLIBRARY_H
#define CTLIBRARY_H

#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <Adafruit_MCP9808.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "CTSettings.h"
#include <WiFi.h>
#include <pthread.h>
#include <vector>

class CTLibrary {
	private:
		int powerPin;
		int A;
		int B;
		int C;
		int D;
		int button1;
		int button2;
		int button3;
		// MCP23017 pins
		int HOUR_A_LD;
		int HOUR_B_LD;
		int MIN_A_LD;
		int MIN_B_LD;
		int TEMP_A_LD;
		int TEMP_B_LD;
		int TEMP_C_LD;
		int TEMP_C_SIGN;
		int TEMP_F_SIGN;
		int TEMP_MINUS;

		float currentCTemperature = -100;
		float currentFTemperature = -100;
		float currentKTemperature = -100;
		int previousHour10 = -1;
		int previousHour1 = -1;
		int previousMinute10 = -1;
		int previousMinute1 = -1;
		int previousDayUpdatedNTP = -1;

		Adafruit_MCP23X17* mcp;
		std::vector<float> temperatureMeasurements;
		CTSettings* settings;
		WiFiServer* wifiServer;
		pthread_t* temperatureServerThread;
		Adafruit_MCP9808* tempsensor;

		void updateTemperatureScreen();
		char* getStringChar(String string);
		void setNumber(int number, int LD);
		bool connectWlan();
		static void *temperatureServer(void *threadid);
		bool powerShouldBeOn();
		void setTemperatureNumber(double temperature);

	public:
		CTLibrary();
		
		void turnOffAllNumbers();
		void setPower();
		void setTemperatureTypeLetter(TemperatureType type);
		CTSettings* getSettings();
		void updateTemperature();
		void updateMySQLTemperatures();
		void updateDateTimeScreen();
		struct tm getCurrentTime();
		unsigned long getCurrentTimeEpoch();
		bool shouldUpdateNTP();
		void updateNTP();
		int millisecondsUntilNextSecond();
		void printLocalTime();
		int getHoursSinceLastMySQLUpdate();
		void createTemperatureServer();
		WiFiServer* getWifiServer();
		float getCurrentCTemperature();
		float getCurrentKTemperature();
		float getCurrentFTemperature();
};

#endif
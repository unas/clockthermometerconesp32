#ifndef TEMPERATURETYPE_H
#define TEMPERATURETYPE_H

enum TemperatureType {
	celsius,
	fahrenheit,
	kelvin
};

#endif
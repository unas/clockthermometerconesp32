#include "CTSettings.h"
#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <Adafruit_MCP9808.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ArduinoJson.h>
#include "TemperatureType.h"
#include <vector>

CTSettings::CTSettings() {

	this->mysqlServerAddress = NULL;
	this->mysqlEnabled = true;
	this->mysqlNeedsAnUpdate = false;

	if (this->setupSD() == false) {
		Serial.println("Couldn't start SD");
	}
	if (this->getSettings() == false) {
		Serial.println("Couldn't get settings!");
	}
}

/**
 * Checks the type of SD card is inserted and initializes the connection
*/
bool CTSettings::setupSD() {
	
	if(!SD.begin(5)){
		Serial.println("Card Mount Failed");
		return false;
	}
	uint8_t cardType = SD.cardType();

	if(cardType == CARD_NONE){
		Serial.println("No SD card attached");
		return false;
	}

	Serial.print("SD Card Type: ");
	if(cardType == CARD_MMC){
		Serial.println("MMC");
	}
	else if(cardType == CARD_SD){
		Serial.println("SDSC");
	}
	else if(cardType == CARD_SDHC){
		Serial.println("SDHC");
	}
	else {
		Serial.println("UNKNOWN");
	}

	uint64_t cardSize = SD.cardSize() / (1024 * 1024);
	Serial.printf("SD Card Size: %lluMB\n", cardSize);

	return true;
}

/**
 * Reads the settings from the SD card. setupSD must be called first!
*/
bool CTSettings::getSettings() {
	bool configSuccess = true;
	StaticJsonDocument<5000> config;
	File file = SD.open("/unbedroom.conf");
	if (!file) {
		Serial.println("Failed to open config file for reading");
		configSuccess = false;
	}
	String fileStr = "";
	while(file.available()){
		char character = file.read();
		fileStr += character;
	}
	DeserializationError error = deserializeJson(config, fileStr);
	if (error) {
		Serial.print(F("deserializeJson() failed for config: "));
		Serial.println(error.f_str());
		configSuccess = false;
	}
	file.close();

	if (config.containsKey("wlan_ssid")) {
		wlanSsid = config.getMember("wlan_ssid").as<const char*>();
		Serial.print("ssid: ");
		Serial.println(wlanSsid);
	}
	else {
		Serial.println("wlan_ssid is missing!");
		configSuccess = false;
	}
	if (config.containsKey("wlan_password")) {
		wlanPassword = config.getMember("wlan_password").as<const char*>();
	}
	else {
		Serial.println("wlan_password is missing!");
		configSuccess = false;
	}
	if (config.containsKey("ntp_server")) {
		ntpServer = config.getMember("ntp_server").as<const char*>();
	}
	else {
		Serial.println("ntp_server is missing!");
		configSuccess = false;
	}
	if (config.containsKey("ntp_timezone")) {
		this->ntpTimezone = config.getMember("ntp_timezone").as<const char*>();
	}
	else {
		Serial.println("ntp_timezone is missing!");
		configSuccess = false;
	}
	if (config.containsKey("temperature_enabled")) {
		temperatureEnabled = config.getMember("temperature_enabled").as<bool>();
	}
	else {
		Serial.println("temperature_enabled is missing!");
		temperatureEnabled = false;
	}
	if (config.containsKey("temperature_server_port")) {
		temperatureServerPort = config.getMember("temperature_server_port").as<int>();
	}
	else {
		Serial.println("temperature_server_port is missing!");
		temperatureServerPort = 5015;
	}
	if (config.containsKey("temperature_type")) {
		String temperatureTypeStr = config.getMember("temperature_type").as<String>();
		if (temperatureTypeStr == "C") temperatureType = celsius;
		else if (temperatureTypeStr == "F") temperatureType = fahrenheit;
		else if (temperatureTypeStr == "K") temperatureType = kelvin;
		else temperatureType = celsius;
	}
	else {
		Serial.println("temperature_type is missing!");
		temperatureType = celsius;
	}
	if (config.containsKey("power") && config["power"].containsKey("start") && config["power"].containsKey("end")) {
		JsonArray powerOnJsonArray = config["power"]["start"];
		if (powerOnJsonArray.size() != 7) {
			Serial.println("power.start array does not contain 7 values!");
			configSuccess = false;
		}
		for (int i = 0; i < powerOnJsonArray.size(); ++i) {
			String timeStr = powerOnJsonArray.getElement(i).as<String>();
			int colonIndex = timeStr.indexOf(':');
			if (colonIndex > 0) {
				String hourStr = timeStr.substring(0, colonIndex);
				String minStr = timeStr.substring(colonIndex + 1);

				double hour = atof(hourStr.c_str());
				double min = atof(minStr.c_str());
				double timeValue = hour + min / 60;
				powerStartTime[i] = timeValue;
			}
			else {
				Serial.print(i);
				Serial.print(": ");
				Serial.print(timeStr);
				Serial.println(" is an invalid time!");
				configSuccess = false;
			}
		}
		JsonArray powerOffJsonArray = config["power"]["end"];
		if (powerOffJsonArray.size() != 7) {
			Serial.println("power.end array does not contain 7 values!");
			configSuccess = false;
		}
		for (int i = 0; i < powerOffJsonArray.size(); ++i) {
			String timeStr = powerOffJsonArray.getElement(i).as<String>();
			int colonIndex = timeStr.indexOf(':');
			if (colonIndex > 0) {
				String hourStr = timeStr.substring(0, colonIndex);
				String minStr = timeStr.substring(colonIndex + 1);

				double hour = atof(hourStr.c_str());
				double min = atof(minStr.c_str());
				double timeValue = hour + min / 60;
				powerEndTime[i] = timeValue;
			}
			else {
				Serial.print(i);
				Serial.print(": ");
				Serial.print(timeStr);
				Serial.println(" is an invalid time!");
				configSuccess = false;
			}
		}
	}
	else {
		Serial.println("Power start and end times are missing!");
		configSuccess = false;
	}
	// MySQL
	if (config.containsKey("mysql_enabled")) {
		mysqlEnabled = config.getMember("mysql_enabled").as<bool>();
	}
	else {
		Serial.println("mysql_enabled is missing!");
		mysqlEnabled = false;
	}
	if (mysqlEnabled) {
		if (config.containsKey("mysql_address")) {
			String mysqlAddressStr = config.getMember("mysql_address").as<const char*>();
			std::vector<int> mysqlAddressVector;
			while (true) {
				int periodIndex = mysqlAddressStr.indexOf('.');
				if (periodIndex >= 0) {
					String mysqlAddressPartStr = mysqlAddressStr.substring(0, periodIndex);
					int mysqlAddressPart = atoi(mysqlAddressPartStr.c_str());
					mysqlAddressVector.push_back(mysqlAddressPart);
					if (mysqlAddressStr.length() > periodIndex) {
						mysqlAddressStr.remove(0, periodIndex + 1);
					}
					else {
						break;
					}
				}
				else {
					int mysqlAddressPart = atoi(mysqlAddressStr.c_str());
					mysqlAddressVector.push_back(mysqlAddressPart);
					break;
				}
				
			}
			if (mysqlAddressVector.size() == 4) {
				mysqlServerAddress = new IPAddress(mysqlAddressVector.at(0), mysqlAddressVector.at(1), mysqlAddressVector.at(2), mysqlAddressVector.at(3));
			}
			else {
				Serial.println("Mysql Server address is faulty!");
				mysqlEnabled = false;
			}
		}
		else {
			Serial.println("mysql_address is missing!");
			mysqlEnabled = false;
		}
		if (config.containsKey("mysql_username")) {
			mysqlUsername = config.getMember("mysql_username").as<const char*>();
		}
		else {
			Serial.println("mysql_username is missing!");
			mysqlEnabled = false;
		}
		if (config.containsKey("mysql_password")) {
			const char* mysqlPasswordConstChar = config.getMember("mysql_password").as<const char*>();
			String mysqlPasswordStr = mysqlPasswordConstChar;
			int mysqlPasswordLength = mysqlPasswordStr.length() + 1;
			char mysqlUsernameChar[mysqlPasswordLength];
			mysqlPasswordStr.toCharArray(mysqlUsernameChar, mysqlPasswordLength);
			mysqlPassword = mysqlUsernameChar;
		}
		else {
			Serial.println("mysql_password is missing!");
			mysqlEnabled = false;
		}
		if (config.containsKey("mysql_port")) {
			mysqlPort = config.getMember("mysql_port").as<int>();
		}
		else {
			Serial.println("mysql_port is missing!");
			mysqlEnabled = false;
		}
		if (config.containsKey("mysql_database")) {
			mysqlDatabase = config.getMember("mysql_database").as<const char*>();
			Serial.print("Mysql database: ");
			Serial.println(mysqlDatabase);
		}
		else {
			Serial.println("mysql_database is missing!");
			mysqlEnabled = false;
		}
		if (config.containsKey("mysql_table")) {
			mysqlTable = config.getMember("mysql_table").as<const char*>();
			Serial.print("Mysql table: ");
			Serial.println(mysqlTable);
		}
		else {
			Serial.println("mysql_table is missing!");
			mysqlEnabled = false;
		}
	}

	return configSuccess;
}

String CTSettings::getWlanSsid() {
	return this->wlanSsid;
}
String CTSettings::getNtpTimezone() {
	return this->ntpTimezone;
}
String CTSettings::getWlanPassword() {
	return this->wlanPassword;
}
String CTSettings::getNtpServer() {
	return this->ntpServer;
}
bool CTSettings::isTemperatureEnabled() {
	return this->temperatureEnabled;
}
int CTSettings::getTemperatureServerPort() {
	return this->temperatureServerPort;
}
double* CTSettings::getPowerStartTime() {
	return this->powerStartTime;
}
double* CTSettings::getPowerEndTime() {
	return this->powerEndTime;
}
TemperatureType CTSettings::getTemperatureType() {
	return this->temperatureType;
}
bool CTSettings::isMysqlEnabled() {
	return this->mysqlEnabled;
}
IPAddress* CTSettings::getMysqlServerAddress() {
	return this->mysqlServerAddress;
}
String CTSettings::getMysqlUsername() {
	return this->mysqlUsername;
}
String CTSettings::getMysqlPassword() {
	return this->mysqlPassword;
}
int CTSettings::getMysqlPort() {
	return this->mysqlPort;
}
String CTSettings::getMysqlDatabase() {
	return this->mysqlDatabase;
}
String CTSettings::getMysqlTable() {
	return this->mysqlTable;
}
void CTSettings::setMysqlNeedsAnUpdate(bool needAnUpdate) {
	this->mysqlNeedsAnUpdate = needAnUpdate;
}
bool CTSettings::isMysqlNeedsAnUpdate() {
	return this->mysqlNeedsAnUpdate;
}
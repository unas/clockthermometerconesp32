#include "CTLibrary.h"
#include <math.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ArduinoJson.h>
#include "TemperatureType.h"
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include <WiFi.h>
#include <pthread.h>
#include <numeric>

CTLibrary::CTLibrary() {		
	this->powerPin = 2;
	this->A = 32;
	this->B = 33;
	this->C = 25;
	this->D = 26;
	this->button1 = 34;
	this->button2 = 35;
	this->button3 = 36;
	// MCP23017 pins
	this->HOUR_A_LD = 7;
	this->HOUR_B_LD = 6;
	this->MIN_A_LD = 5;
	this->MIN_B_LD = 4;
	this->TEMP_A_LD = 8;
	this->TEMP_B_LD = 9;
	this->TEMP_C_LD = 10;
	this->TEMP_C_SIGN = 13;
	this->TEMP_F_SIGN = 12;
	this->TEMP_MINUS = 11;
	
	this->mcp = new Adafruit_MCP23X17();
	//Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();
	this->tempsensor = new Adafruit_MCP9808();
	if (this->tempsensor->begin(0x18) == false) {
		Serial.println("Couldn't find MCP9808! Check your connections and verify the address is correct.");
		while (1000);
	}
	this->tempsensor->setResolution(3);

	this->mcp->begin_I2C();
	if (this->mcp->begin_I2C() == false) {
		Serial.println("Couldn't start I2C");
	}

	// Gettings settings
	this->settings = new CTSettings();

	pinMode(this->powerPin, OUTPUT);
	digitalWrite(this->powerPin, LOW);
	pinMode(this->A, OUTPUT);
	pinMode(this->B, OUTPUT);
	pinMode(this->C, OUTPUT);
	pinMode(this->D, OUTPUT);

	pinMode(this->button1, INPUT);
	pinMode(this->button2, INPUT);
	pinMode(this->button3, INPUT);

	for (int i = 0; i < 16; ++i) {
		this->mcp->pinMode(i, OUTPUT);
		this->mcp->digitalWrite(i, HIGH);
		this->mcp->digitalWrite(i, LOW);
	}
}

/*
Rounds the double parameter and converts to int. Splits the number to 2 seven segment displays
*/
void CTLibrary::setTemperatureNumber(double temperature) {
	
	int roundedTemperature = round(temperature);

	int temp100 = 0;
	int temp10 = 0;
	int temp1 = 0;

	if (this->settings->getTemperatureType() == kelvin) {
		temp100 = roundedTemperature / 100;
		temp10 = roundedTemperature % 100 / 10;
		temp1 = roundedTemperature % 100 % 10;
	}
	else if (this->settings->getTemperatureType() == celsius || this->settings->getTemperatureType() == fahrenheit) {
		temp10 = abs(roundedTemperature / 10);
		temp1 = abs(roundedTemperature % 10);
	}
	if ((this->settings->getTemperatureType() == celsius || this->settings->getTemperatureType() == fahrenheit) && temperature < 0) {
		this->mcp->digitalWrite(TEMP_MINUS, HIGH);
	}
	else {
		mcp->digitalWrite(TEMP_MINUS, LOW);
	}
	if (this->settings->getTemperatureType() == kelvin) {
		setNumber(temp100, TEMP_A_LD);
	}
	setNumber(temp10, TEMP_B_LD);
	setNumber(temp1, TEMP_C_LD);
}

/*
Shows the given integer number (0-9) on the seven segment display connected to the given LD parameter
*/
void CTLibrary::setNumber(int number, int LD) {
	int bitArray[] = {0, 0, 0, 0};
	for (int i = 0; i < 4; ++i) {
		bitArray[3 - i] = bitRead(number, i);
	}

	this->mcp->digitalWrite(LD, HIGH);
	digitalWrite(this->A, bitArray[3]);
	digitalWrite(this->B, bitArray[2]);
	digitalWrite(this->C, bitArray[1]);
	digitalWrite(this->D, bitArray[0]);
	this->mcp->digitalWrite(LD, LOW);
}

/*
Sets all number seven segments to blank on clock and temperature displays
*/
void CTLibrary::turnOffAllNumbers() {

	digitalWrite(this->A, 1);
	digitalWrite(this->B, 1);
	digitalWrite(this->C, 1);
	digitalWrite(this->D, 1);

	this->mcp->digitalWrite(this->HOUR_A_LD, HIGH);
	this->mcp->digitalWrite(this->HOUR_B_LD, HIGH);
	this->mcp->digitalWrite(this->MIN_A_LD, HIGH);
	this->mcp->digitalWrite(this->MIN_B_LD, HIGH);
	this->mcp->digitalWrite(this->TEMP_A_LD, HIGH);
	this->mcp->digitalWrite(this->TEMP_B_LD, HIGH);
	this->mcp->digitalWrite(this->TEMP_C_LD, HIGH);

	this->mcp->digitalWrite(this->HOUR_A_LD, LOW);
	this->mcp->digitalWrite(this->HOUR_B_LD, LOW);
	this->mcp->digitalWrite(this->MIN_A_LD, LOW);
	this->mcp->digitalWrite(this->MIN_B_LD, LOW);
	this->mcp->digitalWrite(this->TEMP_A_LD, LOW);
	this->mcp->digitalWrite(this->TEMP_B_LD, LOW);
	this->mcp->digitalWrite(this->TEMP_C_LD, LOW);
	this->mcp->digitalWrite(this->TEMP_C_SIGN, LOW);
	this->mcp->digitalWrite(this->TEMP_F_SIGN, LOW);
	this->mcp->digitalWrite(this->TEMP_MINUS, LOW);
}

/*
Checks if power should be turned on or off for the displays, then switches the power accordingly
*/
void CTLibrary::setPower() {

	if (powerShouldBeOn()) {
		digitalWrite(powerPin, HIGH);
	}
	else {
		digitalWrite(powerPin, LOW);
	}
}

/*
Returns true/false if power should be on for the displays.
Checks if this current time displays are on, or if a user pressed a button to turn displays on
*/
bool CTLibrary::powerShouldBeOn() {
	bool button1Pressed = digitalRead(this->button1);
	bool button2Pressed = digitalRead(this->button2);
	bool button3Pressed = digitalRead(this->button3);

	struct tm timeinfo = this->getCurrentTime();
	int weekday = timeinfo.tm_wday;
	double currentHour = (double)timeinfo.tm_hour;
	double currentMin = (double)timeinfo.tm_min;
	double currentTime = currentHour + currentMin / 60;
	
	double dayStartTime = this->settings->getPowerStartTime()[weekday];
	double dayEndTime = this->settings->getPowerEndTime()[weekday];

	bool returnValue = currentTime >= dayStartTime && currentTime < dayEndTime;

	return returnValue || button1Pressed || button2Pressed || button3Pressed;
}

CTSettings* CTLibrary::getSettings() {
	return this->settings;
}

/*
Reads the temperature sensor's value through I2C.
Saves the new value to a list.
When 10 values have been saves, the average value will be shown on the display
*/
void CTLibrary::updateTemperature() {
	
	float c = this->tempsensor->readTempC();
	float f = this->tempsensor->readTempF();
	float k = c + 273.15;

	float temp = 0;
	if (this->settings->getTemperatureType() == celsius) temp = c;
	if (this->settings->getTemperatureType() == fahrenheit) temp = f;
	if (this->settings->getTemperatureType() == kelvin) temp = k;

	this->temperatureMeasurements.push_back(temp);
	
	// If program just started (values are -100), then setting current values so that they can
	// be saved to the database, if it has been over an hour
	if (this->currentCTemperature == -100 && this->currentKTemperature == -100 && this->currentFTemperature == -100) {
		this->currentCTemperature = c;
		this->currentKTemperature = k;
		this->currentFTemperature = f;
	}
	
	if (temperatureMeasurements.size() >= 10) {
		float sum = std::accumulate(this->temperatureMeasurements.begin(), this->temperatureMeasurements.end(), 0.0);
		if (this->settings->getTemperatureType() == celsius) {
			this->currentCTemperature = sum / this->temperatureMeasurements.size();
			this->currentFTemperature = this->currentCTemperature * 9 / 5 + 32;
			this->currentKTemperature = this->currentCTemperature + 273.15;
		}
		if (this->settings->getTemperatureType() == fahrenheit) {
			this->currentFTemperature = sum / this->temperatureMeasurements.size();
			this->currentCTemperature = (this->currentFTemperature - 32) * 5 / 9;
			this->currentKTemperature = this->currentCTemperature + 273.15;
		}
		if (this->settings->getTemperatureType() == kelvin) {
			this->currentKTemperature = sum / this->temperatureMeasurements.size();
			this->currentCTemperature = this->currentKTemperature - 273.15;
			this->currentFTemperature = this->currentCTemperature * 9 / 5 + 32;
		}

		this->temperatureMeasurements.clear();

		updateTemperatureScreen();
	}
}

/*
Updates the temperature seven segment displays
*/
void CTLibrary::updateTemperatureScreen() {
	float currentTemp = 0;
	if (this->settings->getTemperatureType() == celsius) currentTemp = currentCTemperature;
	if (this->settings->getTemperatureType() == fahrenheit) currentTemp = currentFTemperature;
	if (this->settings->getTemperatureType() == kelvin) currentTemp = currentKTemperature;

	int roundedTemperature = round(currentTemp);
	
	int temp100 = 0;
	int temp10 = 0;
	int temp1 = 0;
	if (this->settings->getTemperatureType() == kelvin) {
		temp100 = roundedTemperature / 100;
		temp10 = roundedTemperature % 100 / 10;
		temp1 = roundedTemperature % 100 % 10;
	}
	else if (this->settings->getTemperatureType() == celsius || this->settings->getTemperatureType() == fahrenheit) {
		temp10 = abs(roundedTemperature / 10);
		temp1 = abs(roundedTemperature % 10);
	}

	if ((this->settings->getTemperatureType() == celsius || this->settings->getTemperatureType() == fahrenheit) && currentTemp < 0) {
		this->mcp->digitalWrite(TEMP_MINUS, HIGH);
	}
	else {
		this->mcp->digitalWrite(TEMP_MINUS, LOW);
	}
	if (this->settings->getTemperatureType() == kelvin) {
		this->setNumber(temp100, TEMP_A_LD);
	}
	this->setNumber(temp10, TEMP_B_LD);
	this->setNumber(temp1, TEMP_C_LD);
}

/*
Adds a new row to the database with the current temperature
*/
void CTLibrary::updateMySQLTemperatures() {

	String insert_sql = "INSERT INTO ";
	insert_sql += this->settings->getMysqlDatabase();
	insert_sql += ".";
	insert_sql += this->settings->getMysqlTable();
	insert_sql += " (tempC, tempK, tempF) VALUES (";
	insert_sql += this->currentCTemperature;
	insert_sql += ", ";
	insert_sql += this->currentKTemperature;
	insert_sql += ", ";
	insert_sql += this->currentFTemperature;
	insert_sql += ");";
	
	Serial.print("Running sql: ");
	Serial.println(insert_sql);

	WiFiClient sqlClient;
	MySQL_Connection conn((Client *)&sqlClient);

	char* mysqlUsernameChar = getStringChar(this->settings->getMysqlUsername());
	char* mysqlPasswordChar = getStringChar(this->settings->getMysqlPassword());

	if (conn.connect(*this->getSettings()->getMysqlServerAddress(), this->getSettings()->getMysqlPort(), mysqlUsernameChar, mysqlPasswordChar)) {
		Serial.println("Database connected");
	}
	else {
		Serial.println("Database connection failed");
		return;
	}
	MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
	cur_mem->execute(insert_sql.c_str());

	Serial.println("SQL executed");
	delete cur_mem;
	delete mysqlUsernameChar;
	delete mysqlPasswordChar;

	this->settings->setMysqlNeedsAnUpdate(false);
}

/**
 * Returns how many hours since database was previously updated
*/
int CTLibrary::getHoursSinceLastMySQLUpdate() {

	String getDateSQL = "SELECT UNIX_TIMESTAMP(date) as date, id, tempF, tempK, tempC FROM "; //thermometer.temperaturetest order by date desc limit 1;";
	getDateSQL += this->settings->getMysqlDatabase();
	getDateSQL += ".";
	getDateSQL += this->settings->getMysqlTable();
	getDateSQL += " ORDER BY date DESC LIMIT 1;";
	
	Serial.print("Running sql: ");
	Serial.println(getDateSQL);

	WiFiClient sqlClient;
	MySQL_Connection conn((Client *)&sqlClient);

	char* mysqlUsernameChar = getStringChar(this->settings->getMysqlUsername());
	char* mysqlPasswordChar = getStringChar(this->settings->getMysqlPassword());

	if (conn.connect(*this->settings->getMysqlServerAddress(), this->settings->getMysqlPort(), mysqlUsernameChar, mysqlPasswordChar)) {
		Serial.println("Database connected");
	}
	else {
		Serial.println("Database connection failed");
	}
	MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
	cur_mem->execute(getDateSQL.c_str());
	
	cur_mem->get_columns(); // Required, but we won't use it
	row_values* row = cur_mem->get_next_row();
	unsigned long dateEpoch = atol(row->values[0]);
	unsigned long currentEpoch = this->getCurrentTimeEpoch();
	int hours = (currentEpoch - dateEpoch) / (60 * 60);

	Serial.println("SQL get executed");
	delete cur_mem;
	delete mysqlUsernameChar;
	delete mysqlPasswordChar;
	
	return hours;
}

/**
 * Converts String to char*
*/
char* CTLibrary::getStringChar(String string) {

	int stringLength = string.length() + 1;
	char* ch = new char[stringLength];
	string.toCharArray(ch, stringLength);
	
	return ch;
}

/**
 * Checks settings for the temperature type and updates the seven segment display with the correct symbol
*/
void CTLibrary::setTemperatureTypeLetter(TemperatureType type) {

	if (this->settings->getTemperatureType() == celsius) {
		this->mcp->digitalWrite(TEMP_C_SIGN, HIGH);
	}
	else if (this->settings->getTemperatureType() == fahrenheit) {
		this->mcp->digitalWrite(TEMP_C_SIGN, HIGH);
		this->mcp->digitalWrite(TEMP_F_SIGN, HIGH);
	}
	else if (this->settings->getTemperatureType() == kelvin) {
		this->mcp->digitalWrite(TEMP_C_SIGN, LOW);
		this->mcp->digitalWrite(TEMP_F_SIGN, LOW);
	}
}

/**
 * Updates the current time to the seven segment screens
*/
void CTLibrary::updateDateTimeScreen() {
	
	struct tm timeinfo = this->getCurrentTime();

	int hour = timeinfo.tm_hour;
	int hour10 = hour / 10;
	int hour1 = hour % 10;
	int minutes = timeinfo.tm_min;
	int minutes10 = minutes / 10;
	int minutes1 = minutes % 10;

	if (this->previousHour10 != hour10) {
		this->setNumber(hour10, HOUR_A_LD);
		this->previousHour10 = hour10;
	}
	if (this->previousHour1 != hour1) {
		this->setNumber(hour1, HOUR_B_LD);
		// If previousHour1 is negative, then this was the first time setting the hour, so no need to for mysql update
		if (this->previousHour1 >= 0) {
			this->getSettings()->setMysqlNeedsAnUpdate(true);
		}

		this->previousHour1 = hour1;
	}

	if (this->previousMinute10 != minutes10) {
		this->setNumber(minutes10, MIN_A_LD);
		this->previousMinute10 = minutes10;
	}
	if (this->previousMinute1 != minutes1) {
		this->setNumber(minutes1, MIN_B_LD);
		this->previousMinute1 = minutes1;
	}
}

struct tm CTLibrary::getCurrentTime() {

	struct tm timeinfo;
	if(!getLocalTime(&timeinfo)){
		Serial.println("Failed to obtain time");
	}
	return timeinfo;
}

bool CTLibrary::shouldUpdateNTP() {
	return this->getCurrentTime().tm_mday != this->previousDayUpdatedNTP;
}

/**
 * Connects to the NTP server given in settings and updates the system time
*/
void CTLibrary::updateNTP() {
	Serial.println("Updating NTP");
	if (WiFi.isConnected() == false) {
		Serial.println("Wlan isn't connected. Connecting...");
		this->connectWlan();
	}
	configTzTime(this->settings->getNtpTimezone().c_str(), this->settings->getNtpServer().c_str());
	
	this->previousDayUpdatedNTP = getCurrentTime().tm_mday;
}

unsigned long CTLibrary::getCurrentTimeEpoch() {
	time_t now;

	this->getCurrentTime();
	time(&now);
	return now;
}

/**
 * Gets the wlan connection values from settings and connects
*/
bool CTLibrary::connectWlan() {
	bool success = false;
	//connect to WiFi
	Serial.print("Connecting to: ");
	Serial.println(this->settings->getWlanSsid());
	WiFi.begin(this->settings->getWlanSsid().c_str(), this->settings->getWlanPassword().c_str());
	while (WiFi.status() != WL_CONNECTED) {
		if (WiFi.status() == WL_CONNECT_FAILED) {
			Serial.println("Wlan connect fail!");
			success = false;
			break;
		}
		delay(500);
		Serial.print(".");
	}
	Serial.println(" CONNECTED");
	Serial.print("IP Address: ");
	Serial.println(WiFi.localIP().toString());
	
	return success;
}

int CTLibrary::millisecondsUntilNextSecond() {
	struct timeval now;
	gettimeofday(&now, NULL);

	return 1000 - (int)now.tv_usec / 1000;
}

void CTLibrary::printLocalTime() {

	struct tm timeinfo;
	if(!getLocalTime(&timeinfo)){
		Serial.println("Failed to print time");
		return;
	}
	Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

/**
 * Creates a server in a new thread. The server will answer getTemperature requests with the current temperature
*/
void CTLibrary::createTemperatureServer() {
	this->temperatureServerThread = new pthread_t();
	this->wifiServer = new WiFiServer(5015);
	this->wifiServer->begin();
	int threadCreateFail = pthread_create(this->temperatureServerThread, NULL, &CTLibrary::temperatureServer, this);
	if (threadCreateFail) {
		Serial.println("Failed to create temperature server thread!");
	}
}

WiFiServer* CTLibrary::getWifiServer() {
	return this->wifiServer;
}

float CTLibrary::getCurrentCTemperature() {
	return this->currentCTemperature;
}
float CTLibrary::getCurrentKTemperature() {
	return this->currentKTemperature;
}
float CTLibrary::getCurrentFTemperature() {
	return this->currentFTemperature;
}

/**
 * Temperature server for returning the temperature value when wificlient gets a request
*/
void* CTLibrary::temperatureServer(void *ctLibraryVoid) {
	while (true) {
		CTLibrary* ctLibrary = static_cast<CTLibrary*>(ctLibraryVoid);
		WiFiClient client = ctLibrary->getWifiServer()->available();
		if (client) {
			while (client.connected()) {
				String fullMessage = "";
				while (client.available() > 0) {
					char c = client.read();
					fullMessage += c;
				}
				StaticJsonDocument<200> response;
				if (fullMessage == "getTemperature") {
					response["success"] = true;
					response["message"] = "";
					response["temperatureC"] = ctLibrary->getCurrentCTemperature();
					response["temperatureK"] = ctLibrary->getCurrentKTemperature();
					response["temperatureF"] = ctLibrary->getCurrentFTemperature();
				}
				else {
					response["success"] = false;
					response["message"] = "Unknown request!";
				}
				String responseStr;
				serializeJson(response, responseStr);
				Serial.print("Sending response ");
				Serial.println(responseStr);
				client.write(responseStr.c_str());
				delay(10);
			}
			client.stop();
			Serial.println("Client disconnected");
		}
		delay(1000);
	}
}